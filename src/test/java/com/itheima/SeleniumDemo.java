package com.itheima;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {
    public static void main(String[] args) {
        //查看浏览器版本号
        //https://npm.taobao.org/mirrors/chromedriver/ 下载具体版本
        System.setProperty("webdriver.chrome.driver", "D:\\Backup\\Downloads\\chromedriver_win32\\chromedriver.exe");
        // 实例化 Chrome/Chromium 会话
        WebDriver driver = new ChromeDriver();
        driver.get("http://localhost:8080/login");
        System.out.println("The testing page title is: " + driver.getTitle());
        //自动向网页写入名字与密码
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("123456");
        driver.findElement(By.id("btn-ty")).click();
//		driver.quit();	//退出
    }
}
