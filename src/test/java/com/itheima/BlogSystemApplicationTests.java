package com.itheima;

import com.itheima.dao.UserMapper;
import com.itheima.model.domain.User;
import com.itheima.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlogSystemApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    UserMapper userMapper;
    @Autowired
    UserService userService;

    @Test
    public void userTest1(){
        User user = new User();
        user.setUsername("张三");

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String enpass = encoder.encode("123456");
        System.out.println(enpass);
        user.setPassword(enpass);
        user.setCreated(new java.sql.Date(System.currentTimeMillis()));
        user.setValid(1);
        System.out.println(user);
        userMapper.insert(user);

        System.out.println(userMapper.findAll());
    }
    @Test
    public void userServiceTest(){
        User user = new User();
        user.setUsername("张三");
        user.setPassword("123456");
        userService.insert(user);
        System.out.println(userService.findAll());
    }
}
