package com.itheima.dao;

import com.itheima.model.domain.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface  UserMapper {
    // 查询所有用户
    @Select("SELECT * FROM t_user")
    public List<User> findAll();

    // 根据id查询用户信息
    @Select("SELECT * FROM t_user WHERE id=#{id}")
    public User findById(Integer id);

    // 新建用户，同时使用@Options注解获取自动生成的主键id
    @Insert("INSERT INTO t_user (username,password,email,created,valid)" +
            " VALUES (#{username},#{password}, #{email}, #{created}, #{valid})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    public Integer insert(User user);

    // 通过id删除用户
    @Delete("DELETE FROM t_user WHERE id=#{id}")
    public void deleteById(int id);

    //获得自增id
    @Select("SELECT max(id) FROM t_user")
    public Integer getMaxId();

}
