package com.itheima.dao;

import com.itheima.model.domain.User;
import com.itheima.model.domain.UserAuthority;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserAuthorityMapper {
    // 查询所有用户角色
    @Select("SELECT * FROM t_authority")
    public List<UserAuthority> findAll();

    // 根据id查询用户角色
    @Select("SELECT * FROM t_authority WHERE id=#{id}")
    public User findById(Integer id);

    // 新建用户，同时使用@Options注解获取自动生成的主键id
    @Insert("INSERT INTO t_authority (authority)" +
            " VALUES (#{authority})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    public Integer insert(UserAuthority userAuthority);

    // 通过id删除用户
    @Delete("DELETE FROM t_authority WHERE id=#{id}")
    public void deleteById(int id);


}
