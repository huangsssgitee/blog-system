package com.itheima.dao;

import com.itheima.model.domain.User;
import com.itheima.model.domain.UserAuthorityRelation;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserAuthorityRelationMapper {
    // 查询所有用户角色
    @Select("SELECT * FROM t_user_authority")
    public List<UserAuthorityRelation> findAll();

    // 根据id查询用户角色
    @Select("SELECT * FROM t_user_authority WHERE id=#{id}")
    public User findById(Integer id);

    // 新建用户，同时使用@Options注解获取自动生成的主键id
    @Insert("INSERT INTO t_user_authority (user_id,authority_id)" +
            " VALUES (#{user_id},#{authority_id})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    public Integer insert(UserAuthorityRelation userAuthorityRelation);

    // 通过id删除用户
    @Delete("DELETE FROM t_user_authority WHERE id=#{id}")
    public void deleteById(int id);
}
